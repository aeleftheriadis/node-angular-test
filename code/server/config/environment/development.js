'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // Sequelize connector options
  sequelize: {
    /**
     * uri for mysql://root:password@mysql_host.com/database_name
     * @type {String}
     */
    uri: 'mysql://root:auction_pass@192.168.99.100:3306/auctiondb',
    options: {
      logging: false,
      define: {
        timestamps: false
      }
    }
  },

  // Seed database on startup
  seedDB: true

};
